﻿using DesktopCoffee.WinForm.Structs;
using Microsoft.Win32;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DesktopCoffee.WinForm.Views
{
    public partial class MainForm : Form
    {
        private readonly string _AppName = "DesktopCoffee";
        private readonly int _IdleTimeLimit = 150;
        private readonly string _RegistryStartupPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";

        public MainForm()
        {
            this.InitializeComponent();
            this.RunAtStartupToolStripMenuItem.Checked = this._CheckRunAtStartup();
        }

        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LastInputInfo plii);

        [DllImport("Kernel32.dll")]
        private static extern uint GetLastError();

        /// <summary>
        /// Gets the idle time in milliseconds.
        /// </summary>
        /// <returns>Idle time</returns>
        private uint _GetIdleTime()
        {
            var lastInPut = new LastInputInfo();
            lastInPut.cbSize = (uint)Marshal.SizeOf(lastInPut);
            GetLastInputInfo(ref lastInPut);

            return (uint)Environment.TickCount - lastInPut.dwTime;
        }

        private void _MainForm_Load(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void _MainTimer_Tick(object sender, EventArgs e)
        {
            var idleTime = this._GetIdleTime();
            if (idleTime > this._IdleTimeLimit * 1000)
            {
                SendKeys.Send("{CAPSLOCK}");
                SendKeys.Send("{CAPSLOCK}");
                if (this.ShowNotificationsToolStripMenuItem.Checked)
                {
                    this.MainNotifyIcon.ShowBalloonTip(1000, "DesktopCoffee", "Prevented your system from sleeping.", ToolTipIcon.Info);
                }
            }
        }

        private void _RunAtStartupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var toolStripMenuItem = (ToolStripMenuItem)sender;
            if (toolStripMenuItem.Checked)
            {
                this._EnableRunAtStartup();
            }
            else
            {
                this._DisableRunAtStartup();
            }
        }

        private void _ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._DisableRunAtStartup();
            Application.Exit();
        }

        private bool _CheckRunAtStartup()
        {
            using (var registryKey = Registry.CurrentUser.OpenSubKey(this._RegistryStartupPath, true))
            {
                return !string.IsNullOrWhiteSpace(registryKey.GetValue(this._AppName)?.ToString());
            }
        }

        private void _DisableRunAtStartup()
        {
            using (var registryKey = Registry.CurrentUser.OpenSubKey(this._RegistryStartupPath, true))
            {
                registryKey.DeleteValue(this._AppName, false);
            }
        }

        private void _EnableRunAtStartup()
        {
            using (var registryKey = Registry.CurrentUser.OpenSubKey(this._RegistryStartupPath, true))
            {
                registryKey.SetValue(this._AppName, Application.ExecutablePath);
            }
        }
    }
}
