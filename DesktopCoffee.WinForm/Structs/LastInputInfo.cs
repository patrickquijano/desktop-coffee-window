﻿namespace DesktopCoffee.WinForm.Structs
{
    internal struct LastInputInfo
    {
        public uint cbSize;
        public uint dwTime;
    }
}
